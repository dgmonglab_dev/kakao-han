declare module '@capacitor/core' {
  interface PluginRegistry {
    kakaoAuth: kakaoAuthPlugin;
  }
}

export interface kakaoAuthPlugin {
  /*echo(options: { value: string }): Promise<{ value: string }>;
  getContact(filter : string): Promise< { result : any[]} >;*/
  login(): Promise<any>; // login 메서드는 웹에서 할 게 없기 떄문에 Promise 형태의 모든 타입을 리턴할 수 있도록 하겠습니다.
}
