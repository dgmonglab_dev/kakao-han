import { WebPlugin } from '@capacitor/core';
import { kakaoAuthPlugin } from './definitions';

export class kakaoAuthWeb extends WebPlugin implements kakaoAuthPlugin {
  constructor() {
    super({
      name: 'kakaoAuth',
      platforms: ['web'],
    });
  }

 /* async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async getContact(filter: string): Promise<{ result : any[]}> {
    console.log("filter",filter);
    return {
      result: [{
        firstName: 'byeongky',
        secondName: 'Han',
        tel: '010-4069-3773'
      }]
    }
  }*/
  async login() : Promise<any>{
    console.warn("No operation on web platform.");
    return false;
  }

}

const kakaoAuth = new kakaoAuthWeb();

export { kakaoAuth };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(kakaoAuth);
