package com.han.plugins.kakao;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

// 필요 kakao package 로드
import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.KakaoSDK;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.util.exception.KakaoException;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
// 필요 기본 안드로이드 package
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import org.json.JSONException;


@NativePlugin(
        requestCodes = {kakaoAuth.REQUEST_CODE} //플러그인 호출 시 결과값이 올바르면 REQUEST CODE가 함께 리턴됩니다.
)
public class kakaoAuth extends Plugin {
    protected static final int REQUEST_CODE = 1001;   // 플러그인 호출 시 결과값이 올바르면 request_code가 함께 리턴됩니다.
    private static final String LOG_TAG = "KakaoTalk"; // Android Log 를 출력할 때 사용할 태그 필드
    private static volatile Activity currentActivity; // 현재 android activity 를 담을 필드
    private KakaoMeV2ResponseCallback kakaoMeV2ResponseCallback; // 카카오톡에서 제공하는 response callback

    public static Activity getCurrentActivity()
    {
        return currentActivity;
    } // current activity를 리턴하는 getter

    @Override
    public void load() {
        Log.v(LOG_TAG, "kakao : initialize");
        super.load();  // Plugin Class에 load 를 상속받습니다.
        currentActivity = getActivity(); // 현재 activity를 받아옵니다.
        KakaoSDK.init(new KakaoSDKAdapter());  // Kakao 개발문서 내용대로 KakaoSDKAdapter를 아래 만들고, KakaoSDK를 초기화합니다.
        Log.v(LOG_TAG, "kakao : initialize finished");
        String keyHash = com.kakao.util.helper.Utility.getKeyHash(getContext());  // keyhash는 개발중에만 필요하고, 카카오 페이지에 해시 등록을 하기 위해 사용됩니다.
        Log.v(LOG_TAG, keyHash);

    }

    @PluginMethod() // 메서드 이름과 definition 이름을 갖게 한 후 @PluginMethod를 쓰면 자동 매칭
    public void login(final PluginCall call){
        Log.v(LOG_TAG, "Kakao-auth-plugin Login Call");
        removeSessionCallback(); // 유지되고 있는 세션이 있다면 종료
        kakaoMeV2ResponseCallback = new KakaoMeV2ResponseCallback(call) {
            @Override
            public void onFailure(ErrorResult errorResult) {
                loginProcess(call);
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                loginProcess(call);
            }
        };
        UserManagement.getInstance().me(kakaoMeV2ResponseCallback);
        saveCall(call); // 현재 call 된 정보를 저장합니다. 필요시 getSavedCall() 을 사용해 가져올 수 있습니다.
    }

    private void loginProcess(final PluginCall callbackContext) {
        try {
            Session.getCurrentSession().addCallback(new SessionCallback(callbackContext)); // 새로운 세션을 생성합니다.
            openSession(AuthType.KAKAO_TALK); // 세션에 어떤 방식으로 로그인할지 정의합니다. 정의 내용은 Kakao 개발자 문서 참조해주세요.
        } catch (Exception e) {
            e.printStackTrace();
            KakaoCapacitorErrorHandler.errorHandler(callbackContext, new ErrorResult(e));
        }
    }


    private void requestMe(final PluginCall call) {

        Session.getCurrentSession().addCallback(new SessionCallback(call));
        kakaoMeV2ResponseCallback = new KakaoMeV2ResponseCallback(call);
        UserManagement.getInstance().me(kakaoMeV2ResponseCallback);

    }

    private class SessionCallback implements ISessionCallback {

        private PluginCall call;

        public SessionCallback(final PluginCall call) {
            this.call = call;
        }

        @Override
        public void onSessionOpened() {
            requestMe(call);
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {

            if (exception != null) {
                if (exception.toString()
                        .contains("App restarted during Kakao login procedure. Restarting from the start.")) {

                } else {
                    KakaoCapacitorErrorHandler.errorHandler(call, exception.toString());
                }
            }
        }
    }
    private class KakaoMeV2ResponseCallback extends MeV2ResponseCallback { // 기존에 카카오에서 제공하는 CallBack을 상속받습니다.

        private PluginCall call;

        public KakaoMeV2ResponseCallback(final PluginCall call) {
            this.call = call;
        }

        @Override
        public void onFailure(ErrorResult errorResult) {
            KakaoCapacitorErrorHandler.errorHandler(call, errorResult);
        }

        @Override
        public void onSessionClosed(ErrorResult errorResult) {
            KakaoCapacitorErrorHandler.errorHandler(call, errorResult);
            Session.getCurrentSession().checkAndImplicitOpen();
        }

        @Override
        public void onSuccess(MeV2Response response) { // 로그인이 성공되면 전달받은 call을 통해 success 메서드를 호출해서 결과값(토큰)을 사용자에게 전달합니다.
            Log.i("onSuccess", response.toString());
            call.success(handleLoginResult(response, Session.getCurrentSession().getTokenInfo().getAccessToken()));
        }

    }
    private void removeSessionCallback() {
        Session.getCurrentSession().clearCallbacks();
    }

    @Override
    protected void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        super.handleOnActivityResult(requestCode, resultCode, data);
        Log.v(LOG_TAG, "kakao : onActivityResult : " + requestCode + ", code: " + resultCode);
        Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data);
    }
    private JSObject handleLoginResult(MeV2Response meV2Response, String accessToken) {
        Log.v(LOG_TAG, "JSObject : handleLoginResult");
        JSObject response = new JSObject();
        try {
            response = new JSObject(meV2Response.toString());
            response.put("accessToken", accessToken);
            Log.v(LOG_TAG, "kakao response: " + response);
        } catch (JSONException e) {
            Log.v(LOG_TAG, "kakao : handleResult error - " + e.toString());
        }
        return response;
    }
    private static class KakaoSDKAdapter extends KakaoAdapter {

        @Override
        public ISessionConfig getSessionConfig() {
            return new ISessionConfig() {
                @Override
                public AuthType[] getAuthTypes() {
                    return new AuthType[]{AuthType.KAKAO_LOGIN_ALL};
                }

                @Override
                public boolean isUsingWebviewTimer() {
                    return false;
                }

                @Override
                public ApprovalType getApprovalType() {
                    return ApprovalType.INDIVIDUAL;
                }

                @Override
                public boolean isSecureMode() {
                    return false;
                }

                @Override
                public boolean isSaveFormData() {
                    return false;
                }
            };
        }

        @Override
        public IApplicationConfig getApplicationConfig() {
            return new IApplicationConfig() {
                @Override
                public Context getApplicationContext() {
                    return KakaoAuth.getCurrentActivity().getApplicationContext();
                }
            };
        }
    }
    // 카카오톡 인증 세션에 어떤 방식으로 로그인 할지와 현재 activty를 전달해서 카카오톡 로그인창을 오픈합니다.
    public void openSession(final AuthType authType) {
        Session.getCurrentSession().open(authType, currentActivity);
    }


}
